#include <Servo.h>

Servo motor,servo;
int ch1 = 10;                     //Plyn, prava packa, vstup 1100-1900
int ch2 = 9;                      //Natocenie, lava packa, vstup 1100-1900
int ch5 = 8;                      //Prepinac, dole 1004-1007, hore 1996-2000
int M = 5;                        //Motor pin5
int S = 6;                        //Servo riadenia 
int ut1 = 3;                      //ultrazvuk 1 trigger pin
int ue1 = 4;                      //ultrazvuk 1 echo pin
int ut2 = 12;                     //ultrazvuk 2 trigger pin
int ue2 = 13;                     //ultrazvuk 2 echo pin
int ir1 = A0;                     //infracerveny snimac 1-pravy
int ir2 = A1;                     //infracerveny snimac 2-lavy
int ir3 = A2;                     //infracerveny snimac 3-stred
int v=1500,n=90,p=1100;           //v-rychlost, n-natocenie kolies, p-prepinac autonomnosti, vzd-vzdialenost od ultrazvukov

void setup(){
servo.attach(S,1200,1700);        //Pripoj servo na pin S a ocakavaj hodnoty od 1100 do 1900
servo.write(90);                  //Natoc servo na 90 stupnov
motor.attach(M,1200,1700);        //Pripoj motor na pin M a cakaj hodnoty 1100-1900
motor.writeMicroseconds(1500);    //posli na motor hodnotu 1500 (predstavuje nulu na vysielaci)
Serial.begin(9600); 
Serial.flush();
pinMode(ch1,INPUT);
pinMode(ch2,INPUT);
pinMode(ch5,INPUT);
pinMode(ut1,OUTPUT);
pinMode(ue1,INPUT);
pinMode(ut2,OUTPUT);
pinMode(ue2,INPUT);
pinMode(ir1,INPUT);    
pinMode(ir2,INPUT); 
  
}

void loop(){
  if(pulseIn(ch5,HIGH)<1500){
    n=pulseIn(ch1,HIGH);            //zisti hodnotu z vysielaca pre servo
    v=pulseIn(ch2,HIGH);            //zisti hodnotu z vysielaca pre motor
    servo.writeMicroseconds(n);   //zapis hodnotu z vysielaca na servo
    motor.writeMicroseconds(v);   //zapis hodnotu z vysiealca na motor
    posli(v,0,0);
  }                               //ak je prepinac autonomnosti dole (hodnota<1500) pouzivatel ma plnu kontrolu
  else{ 
    int uz1=vzdialenost1();         //ultrazvuk 1
    delay(5);
    int uz2=vzdialenost2();         //ultrazvuk 2
    delay(5);
    int ld1=irs1();                 //laneDetecThor 1
    delay(5);
    int ld2=irs2();                 //laneDetecThor 2
    delay(5);
    int ld3=irs3();                 //laneDetecThor 3
    if(uz2>50 && uz1>50)
    {
      v=1550;   
    }
    if(uz2>25 && uz2<=50 && uz1>25 && uz1<=50)
    {
      v=1525;
    }
    if(uz2<25&&uz1<25)
    {
      v=1500;
    }
      

   if (ld1>500)//pravy ma ciaru 
  {
    n=1900;
  }
  
  if(ld2>500)//lavy ma ciaru
  {
    n=1300;    
  }
  servo.writeMicroseconds(n);
  motor.writeMicroseconds(v);
  posli(v,uz1,uz2);
  }
  }

int vzdialenost1(){
  digitalWrite(ut1, LOW);
  delayMicroseconds(2);
  digitalWrite(ut1, HIGH);
  delayMicroseconds(10);
  digitalWrite(ut1, LOW);
  long cas1=pulseIn(ue1,HIGH);
  int vzd1=cas1*0.034/2;
  return vzd1;
  }
 
 int vzdialenost2(){
  digitalWrite(ut2, LOW);
  delayMicroseconds(2);
  digitalWrite(ut2, HIGH);
  delayMicroseconds(10);
  digitalWrite(ut2, LOW); 
  long cas2=pulseIn(ue2,HIGH);
  int vzd2=cas2*0.034/2;
  return vzd2;
  }
int irs1(){
  int hodnota=analogRead(ir1);
  return hodnota;
  }
  
int irs2(){
  int hodnota=analogRead(ir2);
  return hodnota;
  }
  
int irs3(){
  int hodnota=analogRead(ir3);
  return hodnota;
  }
  
 void posli(int p1, int p2,int p3){
    //Serial.print("v: ");
    Serial.println(p1);
    //Serial.print("uz1: ");
    Serial.println(p2);
    //Serial.print("uz2: ");
    Serial.println(p3);
    }
