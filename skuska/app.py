from threading import Lock
from flask import Flask, render_template, session, request, jsonify, url_for
from flask_socketio import SocketIO, emit, disconnect    
import time
import math
import random
import ConfigParser
import MySQLdb  
import serial
import RPi.GPIO as GPIO
import Adafruit_DHT

config = ConfigParser.ConfigParser()
config.read('config.cfg')
myhost = config.get('mysqlDB', 'host')
myuser = config.get('mysqlDB', 'user')
mypasswd = config.get('mysqlDB', 'passwd')
mydb = config.get('mysqlDB', 'db')

async_mode = None

app = Flask(__name__)

app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock() 


def background_thread(args):
    count = 0
    zadny = 21
    predny=27
    senzorteploty=17
    typ=Adafruit_DHT.DHT11
    GPIO.setmode(GPIO.BCM)       
    GPIO.setup(zadny, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(predny, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    ser = serial.Serial("/dev/ttyACM0", 9600, timeout=None)
    ser.baudrate=9600
    ser.reset_input_buffer()
    ser.reset_output_buffer()
    while True:
        cas = time.time()
        count += 1
        rychlost=int(ser.readline().decode().rstrip())
        vzdialenost1=int(ser.readline().decode().rstrip())
        vzdialenost2=int(ser.readline().decode().rstrip())
        z=GPIO.input(zadny)
        p=GPIO.input(predny)
        vlhkost,teplota=Adafruit_DHT.read_retry(typ,senzorteploty)
        if rychlost<=1510:
            rychlost=0;
        v=round(((rychlost*1700/20000)*0.055)/40,3)
        if z==1 and p==0:
            naklon="hore"
        if z==0 and p==1:
            naklon="dole"
        if z==0 and p==0:
            naklon="rovina"
        if z==1 and p==1:
            naklon="CELE ZLE!"
        print("v=", v)
        print("vzdialenost1=", vzdialenost1)
        print("vzdialenost2=", vzdialenost2)
        socketio.sleep(0.1)
        socketio.emit('my_response',
                      {
                          'count': count,
                          'time':cas,
                          'rychlost':v,
                          'vzdialenost1':vzdialenost1,
                          'vzdialenost2':vzdialenost2,
                          'naklon':naklon,
                          'teplota':teplota,
                          'vlhkost':vlhkost,
                       }, namespace='/test')  
  
@socketio.on('my_event', namespace='/test')
def test_message(message):   
    session['receive_count'] = session.get('receive_count', 0) + 1 
    session['A'] = message['value']    
    emit('my_response',
         {'data': message['value'], 'count': session['receive_count'], 'ampl':1})
    
@socketio.on('connect', namespace='/test')
def test_connect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(target=background_thread, args=session._get_current_object())
    emit('my_response', {'data': 'Connected', 'count': 0})

@app.route('/')
def index():
    return render_template('tabs.html', async_mode=socketio.async_mode)

# Vytvorenie DB
# CREATE TABLE zadanie( id int NOT NULL, data text, PRIMARY KEY(id))
@app.route('/db', methods=['GET', 'POST'])
def dbdata():
    if request.method == "POST":
        data = request.form['data']
        print (data)
        db = MySQLdb.connect(host=myhost,user=myuser,passwd=mypasswd,db=mydb)
        cursor = db.cursor()
        cursor.execute("insert into zadanie(data) values('"+data+"')")
        db.commit()
        return jsonify("oke");

@app.route('/dbdata/<string:num>',methods=['GET', 'POST'])
def dbdata2(num):
    db2=MySQLdb.connect(host=myhost,user=myuser,passwd=mypasswd,db=mydb)
    cursor=db2.cursor()
    print num
    cursor.execute('SELECT data FROM zadanie WHERE id=%s',num)
    rv=cursor.fetchone()
    return str(rv[0])

@app.route('/read/<string:num2>',methods=['GET', 'POST'])
def readmyfile(num2):
    f0=open("static/files/test.txt","r")
    rows=f0.readlines()
    return rows[int(num2)-1]

@app.route('/',methods=['GET','POST'])
def openfile():
    if request.method=="POST":
        data=request.form['data']
        with open("static/files/test.txt","a++") as f:
            f.write(data + "\r\n")
        f.close()
        return jsonify("oke");

@socketio.on('disconnect_request', namespace='/test')
def disconnect_request():
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': 'Disconnected!', 'count': session['receive_count']})
    disconnect()
    
@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected', request.sid)

if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0", port=80, debug=True)
